import boto3, json
ec2 = boto3.resource('ec2')
groups_map = [
  {'groupid': 'sg-025857b6e5eb81d5a', 'refgroupid': 'sg-04fb01973bb222640'},
  {'groupid': 'sg-025857b6e5eb81d5a', 'refgroupid': 'sg-0962e97f5830717b3'},
  {'groupid': 'sg-04fb01973bb222640', 'refgroupid': 'sg-025857b6e5eb81d5a'},
  {'groupid': 'sg-04fb01973bb222640', 'refgroupid': 'sg-0962e97f5830717b3'},
  {'groupid': 'sg-0962e97f5830717b3', 'refgroupid': 'sg-04fb01973bb222640'},
  {'groupid': 'sg-0962e97f5830717b3', 'refgroupid': 'sg-025857b6e5eb81d5a'}
]

for ingress in groups_map:
  security_group = ec2.SecurityGroup(ingress['groupid'])
  for rule in security_group.ip_permissions:
    if "UserIdGroupPairs" in rule.keys() and len(rule["UserIdGroupPairs"]) > 0:
      for useridgrouppair in rule["UserIdGroupPairs"]:
        if useridgrouppair["GroupId"] == ingress['refgroupid']:
          print("aws ec2 revoke-security-group-ingress --group-id " + ingress['groupid'] + " --ip-permissions '[" + json.dumps(rule) + "]'")

for egress in groups_map:
  security_group = ec2.SecurityGroup(egress['groupid'])
  for rule in security_group.ip_permissions_egress:
    if "UserIdGroupPairs" in rule.keys() and len(rule["UserIdGroupPairs"]) > 0:
      for useridgrouppair in rule["UserIdGroupPairs"]:
        if useridgrouppair["GroupId"] == egress['refgroupid']:
          print("aws ec2 revoke-security-group-egress --group-id " + egress['groupid'] + " --ip-permissions '[" + json.dumps(rule) + "]'")

for secgrp in groups_map:
  print("aws ec2 delete-security-group --group-id " + secgrp['groupid'])
