
awsacct_cidr_code = {
  default = "010-074-208-000"
  dev = "010-074-208-000"
  qa = "010-074-240-000"
  prod = "010-075-000-000"
}

ally_application_id = "105211"

application_name = "eda"

service_name = "tf-aws-analytics-s3"

data_classification = "Proprietary"

owner = "analytics"

issrcl_level = "Low"

scm_project = ""

scm_repo = ""

s3_env = {
  dev = "dev"
}

kms_key_alias = {
  dev = "alias/eda-tf-aws-analytics-kms-dev-f-innov-analytics-kms"
  qa = "alias/eda-tf-aws-analytics-kms-qa-r-nonprod-analytics-kms"
  prod = "alias/eda-tf-aws-analytics-kms-prod-analytics-kms"
}

iam_role_id = {
  dev = "AROAUHJVHXJWSHYYCJQ2W"
  qa = "AROA5AQCKSHJJM42CXO4H"
  prod = "AROA3J44ZK2BQ4MTG2FX7"
}

user_workspaces = {
  ws_type = "u"
  root_prefix = "user"
  s3_prefixes = [
    "X_BZ0STC",
    "X_BZ368G",
    "X_BZ5M81",
    "X_BZ5M8L",
    "X_BZ6HC3",
    "X_BZ6YZS",
    "X_BZ763H",
    "X_BZ7JGF",
    "X_BZ94K1",
    "X_BZ97MP",
    "X_BZ9MCC",
    "X_BZ9N4D",
    "X_BZBQMB",
    "X_BZBX2P",
    "X_BZDD56",
    "X_BZFJGG",
    "X_BZFMDC",
    "X_BZG6LB",
    "X_BZJSQT",
    "X_BZJWFS",
    "X_BZJX70",
    "X_BZK603",
    "X_BZL8Z8",
    "X_BZLYYL",
    "X_BZM27V",
    "X_BZS6G1",
    "X_BZSQ55",
    "X_BZT1GF",
    "X_BZTG29",
    "X_BZTH4V",
    "X_BZTHWH",
    "X_BZXVV9",
    "X_BZZYHB",
    "X_CZ0TCS",
    "X_CZ10V7",
    "X_CZ1947",
    "X_CZ4GL7",
    "X_CZ5F7P",
    "X_CZ7QRX",
    "X_CZ8JC7",
    "X_CZ8JXR",
    "X_CZ94LJ",
    "X_CZ9VD9",
    "X_CZBB6B",
    "X_CZCTQK",
    "X_CZCZJ5",
    "X_CZDJT3",
    "X_CZJNR8",
    "X_CZKRKS",
    "X_CZKS66",
    "X_CZNWNB",
    "X_CZP8ZT",
    "X_CZPH18",
    "X_CZQCTJ",
    "X_CZQJZK",
    "X_CZSHFQ",
    "X_CZWNXR",
    "X_CZX913",
    "X_DZ01YQ",
    "X_DZ05B8",
    "X_DZ2D6H",
    "X_DZ2DJL",
    "X_DZ32WJ",
    "X_DZ3HBF",
    "X_DZ6Q6W",
    "X_DZ701N",
    "X_DZ81JN",
    "X_DZ8BR9",
    "X_DZ9BMW",
    "X_DZ9LDJ",
    "X_DZBVDC",
    "X_DZC6HG",
    "X_DZCY7N",
    "X_DZF6BT",
    "X_DZFQ2Q",
    "X_DZGQH3",
    "X_DZGQWS",
    "X_DZGSKJ",
    "X_DZJBSR",
    "X_DZLQBJ",
    "X_DZM57Z",
    "X_DZNWNQ",
    "X_DZNXBV",
    "X_DZNZ99",
    "X_DZP6D6",
    "X_DZPM5K",
    "X_DZQ0QH",
    "X_DZRG2M",
    "X_DZRSBP",
    "X_DZSY6W",
    "X_DZTWDW",
    "X_DZTZPF",
    "X_DZTZSR",
    "X_DZVS9K",
    "X_DZVVV4",
    "X_DZXM2G",
    "X_DZXWYQ",
    "X_FZ11BZ",
    "X_FZ1P3M",
    "X_FZ28R8",
    "X_FZ2WGL",
    "X_FZ2Z2G",
    "X_FZ3SLD",
    "X_FZ3TNZ",
    "X_FZ5GV1",
    "X_FZ748C",
    "X_FZ7D62",
    "X_FZBC1Z",
    "X_FZBC3B",
    "X_FZBGGT",
    "X_FZC7KR",
    "X_FZC7T8",
    "X_FZCRZB",
    "X_FZDJGC",
    "X_FZDKRT",
    "X_FZFSH5",
    "X_FZFYT4",
    "X_FZGGPQ",
    "X_FZH76Y",
    "X_FZJ6RC",
    "X_FZJHZZ",
    "X_FZLH8N",
    "X_FZLK9K",
    "X_FZNGNM",
    "X_FZNSTF",
    "X_FZQHZD",
    "X_FZR9M2",
    "X_FZS5TR",
    "X_FZS6SW",
    "X_FZSCK5",
    "X_FZSKHF",
    "X_FZSKPB",
    "X_FZTL63",
    "X_FZV2P4",
    "X_FZVNRM",
    "X_FZVRL3",
    "X_FZXPBN",
    "X_FZYVTM",
    "X_FZZC7N",
    "X_FZZMQK",
    "X_GZ00HL",
    "X_GZ0V5B",
    "X_GZ1KD2",
    "X_GZ2RHL",
    "X_GZ2V87",
    "X_GZ33G5",
    "X_GZ3SR4",
    "X_GZ40KR",
    "X_GZ4F2W",
    "X_GZ8FN1",
    "X_GZ96G6",
    "X_GZB1YZ",
    "X_GZBQ7P",
    "X_GZCS5R",
    "X_GZCTZK",
    "X_GZD89C",
    "X_GZDW47",
    "X_GZF1DV",
    "X_GZFNG0",
    "X_GZGKV8",
    "X_GZGR10",
    "X_GZGVPK",
    "X_GZHVH9",
    "X_GZJ2YJ",
    "X_GZKBML",
    "X_GZKNK6",
    "X_GZKNMN",
    "X_GZLMZ5",
    "X_GZLVBP",
    "X_GZM2WJ",
    "X_GZMB8T",
    "X_GZNTQQ",
    "X_GZP0WW",
    "X_GZQM9Q",
    "X_GZRQ7M",
    "X_GZT15L",
    "X_GZT2JS",
    "X_GZT7YH",
    "X_GZV2XH",
    "X_GZV3Y2",
    "X_GZWCC6",
    "X_GZXL2V",
    "X_GZXM97",
    "X_GZYFCW",
    "X_HZ083J",
    "X_HZ0K14",
    "X_HZ0RSS",
    "X_HZ0X71",
    "X_HZ1XGP",
    "X_HZ2N26",
    "X_HZ2P5F",
    "X_HZ361X",
    "X_HZ3FM8",
    "X_HZ73X5",
    "X_HZ7660",
    "X_HZ89ZX",
    "X_HZ8RZN",
    "X_HZ980R",
    "X_HZ9YQL",
    "X_HZB806",
    "X_HZBB2C",
    "X_HZC0BW",
    "X_HZFZLR",
    "X_HZH2QD",
    "X_HZHMDJ",
    "X_HZK444",
    "X_HZLM8G",
    "X_HZN9S1",
    "X_HZP29H",
    "X_HZQFXY",
    "X_HZRY22",
    "X_HZSHRT",
    "X_HZSMSL",
    "X_HZSMVY",
    "X_HZTY9T",
    "X_HZV4KQ",
    "X_HZV50K",
    "X_HZXPPF",
    "X_HZYCD3",
    "X_HZYHTP",
    "X_HZZJFG",
    "X_JZ12FL",
    "X_JZ1DD7",
    "X_JZ1G32",
    "X_JZ3LWW",
    "X_JZ3ZX2",
    "X_JZ530Q",
    "X_JZ58ZT",
    "X_JZ59J0",
    "X_JZ6NX9",
    "X_JZ7PS3",
    "X_JZ7W4F",
    "X_JZ7ZZB",
    "X_JZ8KWL",
    "X_JZ94GD",
    "X_JZ96XD",
    "X_JZ9M4X",
    "X_JZCSVJ",
    "X_JZDDZ2",
    "X_JZFX8J",
    "X_JZJBB5",
    "X_JZJP6P",
    "X_JZJY7M",
    "X_JZKBW5",
    "X_JZKZ03",
    "X_JZLM3G",
    "X_JZMWX4",
    "X_JZP8V2",
    "X_JZQ2CZ",
    "X_JZQ8RF",
    "X_JZR4YP",
    "X_JZSRRR",
    "X_JZT8R7",
    "X_JZTZJX",
    "X_JZV66Y",
    "X_JZV9QZ",
    "X_JZVGKX",
    "X_JZW2NH",
    "X_JZXH3H",
    "X_JZXWNR",
    "X_JZYKT2",
    "X_JZZW8C",
    "X_KZ0Z9V",
    "X_KZ2VPS",
    "X_KZ2Z10",
    "X_KZ35QM",
    "X_KZ39RD",
    "X_KZ433X",
    "X_KZ45GL",
    "X_KZ4G3X",
    "X_KZ60HC",
    "X_KZ62XL",
    "X_KZ64ZD",
    "X_KZ69SV",
    "X_KZ7M5R",
    "X_KZ84N4",
    "X_KZ8CHV",
    "X_KZ9LPR",
    "X_KZB2FN",
    "X_KZCR42",
    "X_KZCSM3",
    "X_KZCY3M",
    "X_KZDDKY",
    "X_KZDQM1",
    "X_KZDX5Z",
    "X_KZG42V",
    "X_KZGCLW",
    "X_KZGR0K",
    "X_KZGR84",
    "X_KZGTSR",
    "X_KZGYYC",
    "X_KZHBR2",
    "X_KZHLY2",
    "X_KZJ8WL",
    "X_KZMQ8T",
    "X_KZNDT7",
    "X_KZP7FL",
    "X_KZQWXQ",
    "X_KZQXYC",
    "X_KZTGMB",
    "X_KZVMJT",
    "X_KZVR1D",
    "X_KZVVHY",
    "X_KZZWK9",
    "X_LZ01SB",
    "X_LZ0BXJ",
    "X_LZ0MV3",
    "X_LZ262J",
    "X_LZ2JQZ",
    "X_LZ2SGR",
    "X_LZ2T4M",
    "X_LZ38C0",
    "X_LZ3BVV",
    "X_LZ3L3J",
    "X_LZ3LWQ",
    "X_LZ3T06",
    "X_LZ3TB0",
    "X_LZ4CBP",
    "X_LZ5HN9",
    "X_LZ637B",
    "X_LZ6JJP",
    "X_LZ6JNW",
    "X_LZ73YH",
    "X_LZ7NNZ",
    "X_LZ81W2",
    "X_LZ8ZRM",
    "X_LZB0Z3",
    "X_LZD3P1",
    "X_LZD67T",
    "X_LZDB6T",
    "X_LZF1XN",
    "X_LZFHQC",
    "X_LZJWV8",
    "X_LZJZ1Q",
    "X_LZKX6K",
    "X_LZL2W7",
    "X_LZLL6R",
    "X_LZLXL6",
    "X_LZMV29",
    "X_LZPLPR",
    "X_LZQ1NF",
    "X_LZQXW1",
    "X_LZRH6L",
    "X_LZRR7F",
    "X_LZSW7J",
    "X_LZT61J",
    "X_LZTC17",
    "X_LZTD3T",
    "X_LZV20X",
    "X_LZV6XW",
    "X_LZVBLG",
    "X_LZVS2N",
    "X_LZWVSZ",
    "X_LZXY5L",
    "X_LZYWKP",
    "X_MZ0HM4",
    "X_MZ0ZJR",
    "X_MZ180W",
    "X_MZ1SJ9",
    "X_MZ2PZ9",
    "X_MZ3GX7",
    "X_MZ6NYX",
    "X_MZ6X2Q",
    "X_MZ737S",
    "X_MZ7SC1",
    "X_MZ9GLP",
    "X_MZ9KGZ",
    "X_MZB713",
    "X_MZB88F",
    "X_MZCP6V",
    "X_MZDDL2",
    "X_MZDYZV",
    "X_MZFSTQ",
    "X_MZG089",
    "X_MZGS8B",
    "X_MZGZT5",
    "X_MZH2HB",
    "X_MZJRGP",
    "X_MZK5KK",
    "X_MZK63Y",
    "X_MZKCK3",
    "X_MZLDQK",
    "X_MZLGVZ",
    "X_MZLYJX",
    "X_MZMZQ2",
    "X_MZN4S6",
    "X_MZNL5Z",
    "X_MZPP0X",
    "X_MZPSSC",
    "X_MZQ0VV",
    "X_MZR74B",
    "X_MZS5S9",
    "X_MZSN80",
    "X_MZVG9Z",
    "X_MZXKWT",
    "X_MZXM0J",
    "X_MZXYX3",
    "X_MZYZMR",
    "X_MZZP9D",
    "X_MZZZGQ",
    "X_NZ0J1X",
    "X_NZ0VQ9",
    "X_NZ23Q2",
    "X_NZ2CJH",
    "X_NZ2VPS",
    "X_NZ3TTX",
    "X_NZ460L",
    "X_NZ488Z",
    "X_NZ5YG3",
    "X_NZ6D4X",
    "X_NZ75H4",
    "X_NZ7SL7",
    "X_NZ8277",
    "X_NZ8JWJ",
    "X_NZ9DQL",
    "X_NZ9W9T",
    "X_NZC34X",
    "X_NZCD4Q",
    "X_NZDX8B",
    "X_NZFPBS",
    "X_NZGF82",
    "X_NZGTC2",
    "X_NZJB8K",
    "X_NZJWXY",
    "X_NZJZ2T",
    "X_NZLQFR",
    "X_NZLV92",
    "X_NZM497",
    "X_NZMBYS",
    "X_NZMJNK",
    "X_NZN274",
    "X_NZNL67",
    "X_NZR4KQ",
    "X_NZRK4Q",
    "X_NZRYTF",
    "X_NZS4JP",
    "X_NZSXZL",
    "X_NZV8HF",
    "X_NZVQ88",
    "X_NZVZWM",
    "X_NZXSMQ",
    "X_NZZ91N",
    "X_NZZ99N",
    "X_NZZGVF",
    "X_PZ0LP4",
    "X_PZ10ZX",
    "X_PZ19RK",
    "X_PZ227Q",
    "X_PZ244F",
    "X_PZ2LYC",
    "X_PZ4400",
    "X_PZ6HZT",
    "X_PZ8SLK",
    "X_PZDNCP",
    "X_PZDVKZ",
    "X_PZHK55",
    "X_PZJ254",
    "X_PZJ3P5",
    "X_PZK8QC",
    "X_PZKD4G",
    "X_PZL5ZR",
    "X_PZLYH9",
    "X_PZM1SJ",
    "X_PZM22P",
    "X_PZM31D",
    "X_PZMLG8",
    "X_PZMVFK",
    "X_PZN4B0",
    "X_PZNPGG",
    "X_PZOLP4",
    "X_PZR5ND",
    "X_PZRQBR",
    "X_PZSLKS",
    "X_PZSW95",
    "X_PZTQ90",
    "X_PZTVN8",
    "X_PZW086",
    "X_PZW7V7",
    "X_PZWYLY",
    "X_PZX4L1",
    "X_PZXMJY",
    "X_PZZ1VT",
    "X_PZZDXJ",
    "X_QZ01QN",
    "X_QZ17D6",
    "X_QZ2HKT",
    "X_QZ2ZLN",
    "X_QZ3SNQ",
    "X_QZ3WN8",
    "X_QZ5036",
    "X_QZ5GJX",
    "X_QZ60ZR",
    "X_QZ7SZW",
    "X_QZ8W5M",
    "X_QZ8ZTY",
    "X_QZBY0F",
    "X_QZD47C",
    "X_QZDLDX",
    "X_QZDYP3",
    "X_QZFWJL",
    "X_QZG325",
    "X_QZGV9D",
    "X_QZH4DV",
    "X_QZHNFP",
    "X_QZJJWR",
    "X_QZKDJ4",
    "X_QZKFYZ",
    "X_QZKL57",
    "X_QZKZFC",
    "X_QZL21F",
    "X_QZLN56",
    "X_QZLQ5L",
    "X_QZM0LX",
    "X_QZN5RR",
    "X_QZN6KB",
    "X_QZNNP3",
    "X_QZP026",
    "X_QZPN9H",
    "X_QZR3JW",
    "X_QZS215",
    "X_QZSP7F",
    "X_QZSV93",
    "X_QZT8ZY",
    "X_QZTHFH",
    "X_QZVZ2P",
    "X_QZWJNB",
    "X_QZX3YB",
    "X_QZXBGQ",
    "X_QZYF95",
    "X_QZZ8WL",
    "X_QZZ9VZ",
    "X_RZ1G2B",
    "X_RZ29MV",
    "X_RZ2QCS",
    "X_RZ4P7D",
    "X_RZ4XVR",
    "X_RZ5YV3",
    "X_RZ6YKQ",
    "X_RZ8591",
    "X_RZ85SF",
    "X_RZ87KP",
    "X_RZ8LCH",
    "X_RZ93Z4",
    "X_RZ94HV",
    "X_RZ9FHY",
    "X_RZ9GLM",
    "X_RZ9LVB",
    "X_RZB1D1",
    "X_RZBS9J",
    "X_RZBSR3",
    "X_RZC84R",
    "X_RZC85T",
    "X_RZCSND",
    "X_RZDKYV",
    "X_RZDZGM",
    "X_RZF37S",
    "X_RZFGGZ",
    "X_RZFZZM",
    "X_RZGYYG",
    "X_RZHFNW",
    "X_RZHKTY",
    "X_RZJMN8",
    "X_RZLPQM",
    "X_RZNXS5",
    "X_RZP8LS",
    "X_RZPM8R",
    "X_RZQQC9",
    "X_RZQY10",
    "X_RZRKYM",
    "X_RZRMS2",
    "X_RZRS6T",
    "X_RZS4LR",
    "X_RZSWFM",
    "X_RZTXRV",
    "X_RZW6C4",
    "X_RZWHM5",
    "X_RZWYTL",
    "X_RZXPB8",
    "X_RZXZ7J",
    "X_RZYQXT",
    "X_RZZXM2",
    "X_SZ07Z9",
    "X_SZ0BDH",
    "X_SZ0RMD",
    "X_SZ11HF",
    "X_SZ3KFP",
    "X_SZ61F9",
    "X_SZ664W",
    "X_SZ7N21",
    "X_SZ7ZM5",
    "X_SZ8MBL",
    "X_SZB8KS",
    "X_SZCSRK",
    "X_SZCXZQ",
    "X_SZD9C5",
    "X_SZDQ69",
    "X_SZFBST",
    "X_SZFC5H",
    "X_SZHM57",
    "X_SZHTH5",
    "X_SZJ2KG",
    "X_SZJ30M",
    "X_SZJXHK",
    "X_SZJXZ3",
    "X_SZKHGZ",
    "X_SZKS3W",
    "X_SZKXZ9",
    "X_SZLM35",
    "X_SZLQZY",
    "X_SZNQ3R",
    "X_SZPMKL",
    "X_SZPWM9",
    "X_SZQ9VY",
    "X_SZR2BX",
    "X_SZRMVP",
    "X_SZSH9X",
    "X_SZTD7S",
    "X_SZTNLR",
    "X_SZXZ3D",
    "X_SZYPHX",
    "X_SZYTKQ",
    "X_SZZ3DK",
    "X_SZZYMT",
    "X_TZ00ND",
    "X_TZ0R02",
    "X_TZ10G5",
    "X_TZ29RW",
    "X_TZ2D4X",
    "X_TZ2KMN",
    "X_TZ551V",
    "X_TZ57QZ",
    "X_TZ5D61",
    "X_TZ61JZ",
    "X_TZ6244",
    "X_TZ7PXP",
    "X_TZ8FGX",
    "X_TZ9R72",
    "X_TZ9WC2",
    "X_TZBK4W",
    "X_TZBSLS",
    "X_TZBTNC",
    "X_TZCY57",
    "X_TZDGYJ",
    "X_TZDXTB",
    "X_TZHBZC",
    "X_TZHDSD",
    "X_TZHMR3",
    "X_TZKFSX",
    "X_TZKH7Z",
    "X_TZMTWY",
    "X_TZN5BH",
    "X_TZNZBL",
    "X_TZP0VP",
    "X_TZP3V1",
    "X_TZQ3DT",
    "X_TZS4GG",
    "X_TZSYZD",
    "X_TZVKJN",
    "X_TZVTDC",
    "X_TZX27C",
    "X_TZZCMG",
    "X_TZZXKM",
    "X_VZ04SG",
    "X_VZ0CK2",
    "X_VZ0KRK",
    "X_VZ2DZY",
    "X_VZ2L26",
    "X_VZ4TC0",
    "X_VZ6G8X",
    "X_VZ78Z1",
    "X_VZ87ZN",
    "X_VZ88FZ",
    "X_VZ9B06",
    "X_VZ9R4W",
    "X_VZBC4Z",
    "X_VZBJ66",
    "X_VZDSD9",
    "X_VZDSPX",
    "X_VZFDWH",
    "X_VZFHV0",
    "X_VZGGRQ",
    "X_VZHR7C",
    "X_VZHVYQ",
    "X_VZJT9P",
    "X_VZJZFB",
    "X_VZLDTT",
    "X_VZLRF1",
    "X_VZM6JN",
    "X_VZN5YT",
    "X_VZQKF8",
    "X_VZRMMG",
    "X_VZSZ71",
    "X_VZTMKK",
    "X_VZWFFN",
    "X_VZWJZ4",
    "X_VZWKS5",
    "X_VZY20M",
    "X_VZYWF2",
    "X_VZZ2Z9",
    "X_WZ0203",
    "X_WZ0PSY",
    "X_WZ0T6C",
    "X_WZ0XQX",
    "X_WZ1P1P",
    "X_WZ21K3",
    "X_WZ2SGM",
    "X_WZ2WDD",
    "X_WZ42N4",
    "X_WZ4DC3",
    "X_WZ4KJG",
    "X_WZ4YLQ",
    "X_WZ5TCK",
    "X_WZ6J9J",
    "X_WZ77JT",
    "X_WZ7F8V",
    "X_WZBQ8P",
    "X_WZBTXM",
    "X_WZCBJT",
    "X_WZCBT7",
    "X_WZG7CR",
    "X_WZGMGN",
    "X_WZHS39",
    "X_WZJCKD",
    "X_WZK3PP",
    "X_WZKD2W",
    "X_WZKV49",
    "X_WZMQYS",
    "X_WZN485",
    "X_WZNK8F",
    "X_WZNR88",
    "X_WZP0HP",
    "X_WZQXTX",
    "X_WZS4K1",
    "X_WZSB5B",
    "X_WZV3M1",
    "X_WZV72Q",
    "X_WZV917",
    "X_WZWVC2",
    "X_WZXQLH",
    "X_WZXVRQ",
    "X_WZYCGS",
    "X_WZZS82",
    "X_XZ0XH2",
    "X_XZ10MB",
    "X_XZ3KZP",
    "X_XZ3MFB",
    "X_XZ47SX",
    "X_XZ4BG6",
    "X_XZ4VTB",
    "X_XZ51PJ",
    "X_XZ5MFX",
    "X_XZ5RQZ",
    "X_XZ9SBF",
    "X_XZ9XRW",
    "X_XZBDY7",
    "X_XZBQSJ",
    "X_XZDFQV",
    "X_XZDZKY",
    "X_XZF82C",
    "X_XZFN2W",
    "X_XZGFQV",
    "X_XZHZF8",
    "X_XZHZS9",
    "X_XZJ5BC",
    "X_XZJ90R",
    "X_XZL77Y",
    "X_XZM5F8",
    "X_XZMM0R",
    "X_XZMXJW",
    "X_XZN5LQ",
    "X_XZP0G1",
    "X_XZP2S3",
    "X_XZT8PZ",
    "X_XZTKK0",
    "X_XZTX1V",
    "X_XZXL10",
    "X_XZY9WL",
    "X_XZYDN9",
    "X_ZZ1LD3",
    "X_ZZ2QL6",
    "X_ZZ4LYY",
    "X_ZZ5MCN",
    "X_ZZ60TY",
    "X_ZZ6CXJ",
    "X_ZZ6L7C",
    "X_ZZ87W4",
    "X_ZZ8DWF",
    "X_ZZB08G",
    "X_ZZC7KS",
    "X_ZZGTWC",
    "X_ZZHC7J",
    "X_ZZHLPP",
    "X_ZZJFGS",
    "X_ZZJYJ1",
    "X_ZZKCFL",
    "X_ZZP0BT",
    "X_ZZPRP6",
    "X_ZZQ6K0",
    "X_ZZQRK0",
    "X_ZZR2F3",
    "X_ZZR640",
    "X_ZZR6RM",
    "X_ZZRY0K",
    "X_ZZTDD0",
    "X_ZZVMWQ",
    "X_ZZVRHR",
    "X_ZZW1B3"
  ]
}

team_workspaces = {
  ws_type = "t"
  root_prefix = "team"
  team_configs = {
    TECH-PLTFRM-AWS = {
      name = "TECH-PLTFRM-AWS"
      env_scope = [
        "innov",
        "dev",
        "prod"
      ]
      s3_prefixes = ["TECH_PLATFORM_AWS"]
    }
    AUTO-BUS-OPTI = {
      name = "AUTO-BUS-OPTI"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_BUSINESS_OPTIMIZATION"]
    }
    AUTO-DSS = {
      name = "AUTO-DSS"
      env_scope = ["dev", "prod"]
      s3_prefixes = ["AUTO_DSS"]
    }
    AUTO-DSS-CS-LP = {
      name = "AUTO-DSS-CS-LP"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_DSS_CAM_CS_LP"]
    }
    AUTO-DSS-DIG = {
      name = "AUTO-DSS-DIG"
      env_scope = [
        "dev",
        "sit",
        "model",
        "cap",
        "prod"
      ]
      s3_prefixes = ["AUTO_DSS_DIGITAL"]
    }
    AUTO-INSU-SPA = {
      name = "AUTO-INSU-SPA"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_INSURANCE_SPA"]
    }
    TECH-CIH = {
      name = "TECH-CIH"
      env_scope = [
        "dev",
        "sit",
        "prod"
      ]
      s3_prefixes = ["TECH_CIH"]
    }
    TECH-QMA = {
      name = "TECH-QMA"
      env_scope = ["prod"]
      s3_prefixes = ["TECH_QMA"]
    }
    TECH-MEAD = {
      name = "TECH-MEAD"
      env_scope = ["dev", "prod"]
      s3_prefixes = ["TECH_MEAD"]
    }
    TECH-PLTFRM-NUC = {
      name = "TECH-PLTFRM-NUC"
      env_scope = ["dev", "prod"]
      s3_prefixes = ["TECH_PLATFORM_NUCLEUS"]
    }
    ATA-BTCMP = {
      name = "ATA-BTCMP"
      env_scope = ["dev"]
      s3_prefixes = ["ATA_BOOTCAMP"]
    }
    AUDT-SER = {
      name = "AUDT-SER"
      env_scope = ["prod"]
      s3_prefixes = ["AUDIT_SERVICES"]
    }
    AUTO-ADV-OPTI = {
      name = "AUTO-ADV-OPTI"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_ADVANCED_PROCESS_OPTIMIZATION"]
    }
    AUTO-AUTOVAL = {
      name = "AUTO-AUTOVAL"
      env_scope = ["dev", "prod"]
      s3_prefixes = ["AUTO_AUTOVAL"]
    }
    AUTO-CAM-STR = {
      name = "AUTO-CAM-STR"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_CAM_STRATEGY"]
    }
    AUTO-CL-BLCR = {
      name = "AUTO-CL-BLCR"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_CL_BLCR"]
    }
    AUTO-COSA = {
      name = "AUTO-COSA"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_COSA"]
    }
    AUTO-COSA-AA = {
      name = "AUTO-COSA-AA"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_COSA_AA"]
    }
    AUTO-COSA-ADS = {
      name = "AUTO-COSA-ADS"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_COSA_ADS"]
    }
    AUTO-COSA-CCO = {
      name = "AUTO-COSA-CCO"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_COSA_CCO"]
    }
    AUTO-COSA-CP = {
      name = "AUTO-COSA-CP"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_COSA_CP"]
    }
    AUTO-COSA-NDLDG = {
      name = "AUTO-COSA-NDLDG"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_COSA_NDLDG"]
    }
    AUTO-COSA-PRM = {
      name = "AUTO-COSA-PRM"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_COSA_PRM"]
    }
    AUTO-COSA-SASO = {
      name = "AUTO-COSA-SASO"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_COSA_SASO"]
    }
    AUTO-CRD-OPS = {
      name = "AUTO-CRD-OPS"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_CREDIT_OPS"]
    }
    AUTO-DIR = {
      name = "AUTO-DIR"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_DIRECT"]
    }
    AUTO-DLR-MGMT = {
      name = "AUTO-DLR-MGMT"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_DLR_MGMT"]
    }
    AUTO-DSS-ARC-SSO = {
      name = "AUTO-DSS-ARC-SSO"
      env_scope = [
        "dev",
        "sit",
        "cap",
        "prod"
      ]
      s3_prefixes = ["AUTO_DSS_ARC_SSO"]
    }
    AUTO-DSS-CON-STR = {
      name = "AUTO-DSS-CON-STR"
      env_scope = [
        "dev",
        "sit",
        "model",
        "cap",
        "prod"
      ]
      s3_prefixes = ["AUTO_DSS_CONS_STRATEGY"]
    }
    AUTO-DSS-DA = {
      name = "AUTO-DSS-DA"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_DSS_DA"]
    }
    AUTO-DSS-DIR = {
      name = "AUTO-DSS-DIR"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_DSS_DIRECT"]
    }
    AUTO-DSS-ORIG = {
      name = "AUTO-DSS-ORIG"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_DSS_ORIG"]
    }
    AUTO-DSS-SLS = {
      name = "AUTO-DSS-SLS"
      env_scope = ["dev", "prod"]
      s3_prefixes = ["AUTO_DSS_SLS"]
    }
    AUTO-DSS-UW = {
      name = "AUTO-DSS-UW"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_DSS_UW"]
    }
    AUTO-ICP = {
      name = "AUTO-ICP"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_ICP"]
    }
    AUTO-PRTFL-RSK = {
      name = "AUTO-PRTFL-RSK"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_PORTFOLIO_RISK"]
    }
    AUTO-PRC = {
      name = "AUTO-PRC"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_PRICING"]
    }
    AUTO-RMRKT-3PR = {
      name = "AUTO-RMRKT-3PR"
      env_scope = ["model", "prod"]
      s3_prefixes = ["AUTO_REMARKETING_3PR"]
    }
    AUTO-RSK-FRCST = {
      name = "AUTO-RSK-FRCST"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_RISK_FORECAST"]
    }
    AUTO-SLSALLNCE = {
      name = "AUTO-SLSALLNCE"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_SALESALLIANCE"]
    }
    AUTO-SP-ADR = {
      name = "AUTO-SP-ADR"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_SP_ADR"]
    }
    AUTO-SP-ICP = {
      name = "AUTO-SP-ICP"
      env_scope = ["prod"]
      s3_prefixes = ["AUTO_SP_ICP"]
    }
    BANK-CCERA = {
      name = "BANK-CCERA"
      env_scope = ["dev", "prod"]
      s3_prefixes = ["BANK_CCERA"]
    }
    BANK-CON = {
      name = "BANK-CON"
      env_scope = ["dev", "prod"]
      s3_prefixes = ["BANK_CONSUMER"]
    }
    BANK-CX-CC = {
      name = "BANK-CX-CC"
      env_scope = ["model", "prod"]
      s3_prefixes = ["BANK_CX_CC"]
    }
    BANK-DEP-OPS = {
      name = "BANK-DEP-OPS"
      env_scope = ["model", "prod"]
      s3_prefixes = ["BANK_DEPOSIT_OPS"]
    }
    BANK-DSS-DEPINV = {
      name = "BANK-DSS-DEPINV"
      env_scope = [
        "dev",
        "sit",
        "model",
        "cap",
        "prod"
      ]
      s3_prefixes = ["BANK_DSS_DEPINV"]
    }
    BANK-FRD-STR = {
      name = "BANK-FRD-STR"
      env_scope = ["model", "prod"]
      s3_prefixes = ["BANK_FRAUD_STRATEGY"]
    }
    BANK-INV = {
      name = "BANK-INV"
      env_scope = ["model", "prod"]
      s3_prefixes = ["BANK_INVEST"]
    }
    BANK-MORT-BA = {
      name = "BANK-MORT-BA"
      env_scope = ["prod"]
      s3_prefixes = ["BANK_MORT_BA"]
    }
    COMP-AML = {
      name = "COMP-AML"
      env_scope = [
        "dev",
        "sit",
        "model",
        "cap",
        "prod"
      ]
      s3_prefixes = ["COMPLIANCE_AML"]
    }
    COMP-EFSI = {
      name = "COMP-EFSI"
      env_scope = [
        "dev",
        "sit",
        "model",
        "cap",
        "prod"
      ]
      s3_prefixes = ["COMPLIANCE_EFSI"]
    }
    FIN-ACC-ENG = {
      name = "FIN-ACC-ENG"
      env_scope = [
        "dev",
        "sit",
        "model",
        "cap",
        "prod"
      ]
      s3_prefixes = ["FINANCE_ACCOUNTING_ENG"]
    }
    FIN-AUTO-CFO = {
      name = "FIN-AUTO-CFO"
      env_scope = ["prod"]
      s3_prefixes = ["FINANCE_AUTO_CFO"]
    }
    FIN-CPTL-MRKTS = {
      name = "FIN-CPTL-MRKTS"
      env_scope = [
        "dev",
        "model",
        "prod"
      ]
      s3_prefixes = ["FINANCE_CPTL_MARKETS"]
    }
    FIN-ECNMCS = {
      name = "FIN-ECNMCS"
      env_scope = ["dev", "prod"]
      s3_prefixes = ["FINANCE_ECONOMICS"]
    }
    FIN-FMA-MDL = {
      name = "FIN-FMA-MDL"
      env_scope = ["prod"]
      s3_prefixes = ["FINANCE_FMA_MODEL"]
    }
    FIN-GL-SAP = {
      name = "FIN-GL-SAP"
      env_scope = [
        "dev",
        "sit",
        "model",
        "cap",
        "prod"
      ]
      s3_prefixes = ["FINANCE_GL_SAP"]
    }
    FIN-SPLY-CHN = {
      name = "FIN-SPLY-CHN"
      env_scope = [
        "dev",
        "model",
        "prod"
      ]
      s3_prefixes = ["FINANCE_SUPPLY_CHAIN"]
    }
    FIN-VAR-LIB = {
      name = "FIN-VAR-LIB"
      env_scope = [
        "dev",
        "sit",
        "model",
        "cap",
        "prod"
      ]
      s3_prefixes = ["FINANCE_VARIABLES_LIBRARY"]
    }
    HR-PEP-SYS = {
      name = "HR-PEP-SYS"
      env_scope = ["prod"]
      s3_prefixes = ["HR_PEOPLE_SYSTEMS"]
    }
    HR-WRKFRC = {
      name = "HR-WRKFRC"
      env_scope = ["prod"]
      s3_prefixes = ["HR_WORKFORCE"]
    }
    INS-DWR = {
      name = "INS-DWR"
      env_scope = ["model", "prod"]
      s3_prefixes = ["INSURANCE_DWR"]
    }
    IRM-AUTO-CRD = {
      name = "IRM-AUTO-CRD"
      env_scope = ["prod"]
      s3_prefixes = ["IRM_AUTO_CREDIT"]
    }
    IRM-CARD = {
      name = "IRM-CARD"
      env_scope = ["prod"]
      s3_prefixes = ["IRM_CARD"]
    }
    IRM-ESG = {
      name = "IRM-ESG"
      env_scope = ["prod"]
      s3_prefixes = ["IRM_ESG"]
    }
    IRM-INS-RSK = {
      name = "IRM-INS-RSK"
      env_scope = ["prod"]
      s3_prefixes = ["IRM_INSURANCE_RISK"]
    }
    MKTG-CRM = {
      name = "MKTG-CRM"
      env_scope = ["prod"]
      s3_prefixes = ["MARKETING_CRM"]
    }
    MKTG-DIG = {
      name = "MKTG-DIG"
      env_scope = ["dev", "prod"]
      s3_prefixes = ["MARKETING_DIGITAL"]
    }
    TECH-ADV = {
      name = "TECH-ADV"
      env_scope = ["prod"]
      s3_prefixes = ["TECH_ADVANTAGE"]
    }
    TECH-COMP-CRT = {
      name = "TECH-COMP-CRT"
      env_scope = ["dev"]
      s3_prefixes = ["TECH_COMPLIANCE_CRT"]
    }
    TECH-COMP-DENG = {
      name = "TECH-COMP-DENG"
      env_scope = [
        "dev",
        "sit",
        "model",
        "cap",
        "prod"
      ]
      s3_prefixes = ["TECH_COMPLIANCE_DATAENG"]
    }
    TECH-COMP-FRD-CM = {
      name = "TECH-COMP-FRD-CM"
      env_scope = [
        "dev",
        "sit",
        "model",
        "cap",
        "prod"
      ]
      s3_prefixes = ["TECH_COMPLIANCE_FRAUD_CASEMGMT"]
    }
    TECH-COMP-SUS = {
      name = "TECH-COMP-SUS"
      env_scope = [
        "dev",
        "sit",
        "model",
        "cap",
        "prod"
      ]
      s3_prefixes = ["TECH_COMPLIANCE_SUSTAIN"]
    }
    TECH-CONVOAI = {
      name = "TECH-CONVOAI"
      env_scope = ["prod"]
      s3_prefixes = ["TECH_CONVOAI"]
    }
    TECH-DGO = {
      name = "TECH-DGO"
      env_scope = ["dev", "prod"]
      s3_prefixes = ["TECH_DGO"]
    }
    TECH-EA-DATA = {
      name = "TECH-EA-DATA"
      env_scope = ["prod"]
      s3_prefixes = ["TECH_EA_DATA"]
    }
    TECH-EDA-BD-SER = {
      name = "TECH-EDA-BD-SER"
      env_scope = [
        "dev",
        "sit",
        "model",
        "cap",
        "prod"
      ]
      s3_prefixes = ["TECH_EDA_BANK_DATA_SERVICES"]
    }
    TECH-EDA-SOL-ENG = {
      name = "TECH-EDA-SOL-ENG"
      env_scope = ["dev", "prod"]
      s3_prefixes = ["TECH_EDA_SOLUTION_ENG"]
    }
    TECH-FUS = {
      name = "TECH-FUS"
      env_scope = ["dev", "prod"]
      s3_prefixes = ["TECH_FUSION"]
    }
    TECH-IPRM-CRSK = {
      name = "TECH-IPRM-CRSK"
      env_scope = ["dev", "prod"]
      s3_prefixes = ["TECH_IPRM_CYBERRISK"]
    }
    TECH-SUS-EDA = {
      name = "TECH-SUS-EDA"
      env_scope = ["dev"]
      s3_prefixes = ["TECH_SUSTAIN_EDA"]
    }
  }
}

s3_sub_prefixes = [
  "WORK",
  "TEST",
  "CORE",
  "STATIC"
]
