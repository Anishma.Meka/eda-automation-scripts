awsacct_cidr_code = {
  default = "010-074-208-000"
  dev     = "010-074-208-000"
  qa      = "010-074-240-000"
  prod    = "010-075-000-000"
}

ally_application_id = "105211"
application_name    = "eda"
service_name        = "tf-aws-analytics-s3"
data_classification = "Proprietary"
owner               = "analytics"
issrcl_level        = "Low"
scm_project         = ""
scm_repo            = ""

s3_env = {
  dev = "dev"
}

kms_key_alias = {
  dev  = "alias/eda-tf-aws-analytics-kms-dev-f-innov-analytics-kms"
  qa   = "alias/eda-tf-aws-analytics-kms-qa-r-nonprod-analytics-kms"
  prod = "alias/eda-tf-aws-analytics-kms-prod-analytics-kms"
}

iam_role_id = {
  dev  = "AROAUHJVHXJWSHYYCJQ2W"
  qa   = "AROA5AQCKSHJJM42CXO4H"
  prod = "AROA3J44ZK2BQ4MTG2FX7"
}

user_workspaces = {
  ws_type     = "u"
  root_prefix = "user"
  s3_prefixes = [
    "X_BZ368G", "X_BZ6YZS", "X_BZDD56", "X_BZJX70", "X_BZL8Z8", "X_BZZYHB", "X_CZ0TCS",
    "X_CZ7QRX", "X_CZNWNB", "X_CZQJZK", "X_DZ2DJL", "X_DZ9BMW", "X_DZF6BT", "X_DZNXBV",
    "X_DZP6D6", "X_DZPM5K", "X_DZSY6W", "X_FZ3TNZ", "X_FZ748C", "X_FZBC3B", "X_FZCRZB",
    "X_FZH76Y", "X_FZJHZZ", "X_FZS6SW", "X_FZZC7N", "X_GZ0V5B", "X_GZBQ7P", "X_GZGR10",
    "X_GZP0WW", "X_GZXL2V", "X_GZYFCW", "X_HZ0X71", "X_HZ1XGP", "X_HZ2N26", "X_HZ361X",
    "X_HZ73X5", "X_HZ7660", "X_HZ89ZX", "X_HZ980R", "X_HZ9YQL", "X_HZB806", "X_HZC0BW",
    "X_HZHMDJ", "X_HZP29H", "X_HZYHTP", "X_JZ3LWW", "X_JZ6NX9", "X_JZCSVJ", "X_JZLM3G",
    "X_JZVGKX", "X_JZYKT2", "X_KZ2VPS", "X_KZ2Z10", "X_KZ60HC", "X_KZ8CHV", "X_KZCY3M",
    "X_KZGR84", "X_KZMQ8T", "X_LZ0MV3", "X_LZ38C0", "X_LZ3T06", "X_LZ6JNW", "X_LZ7NNZ",
    "X_LZB0Z3", "X_LZDB6T", "X_LZJZ1Q", "X_LZYWKP", "X_MZ3GX7", "X_MZ6NYX", "X_MZ7SC1",
    "X_MZJRGP", "X_MZKCK3", "X_MZLDQK", "X_MZQ0VV", "X_MZZZGQ", "X_NZ0J1X", "X_NZ2CJH",
    "X_NZ2VPS", "X_NZ3TTX", "X_NZ75H4", "X_NZGF82", "X_NZLQFR", "X_NZMBYS", "X_NZVZWM",
    "X_NZXSMQ", "X_NZZ91N", "X_PZ244F", "X_PZ6HZT", "X_PZ8SLK", "X_PZJ254", "X_PZJ3P5",
    "X_PZK8QC", "X_PZM1SJ", "X_PZM31D", "X_PZMVFK", "X_PZSLKS", "X_PZTVN8", "X_PZW7V7",
    "X_PZZDXJ", "X_QZ2ZLN", "X_QZ60ZR", "X_QZD47C", "X_QZFWJL", "X_QZH4DV", "X_QZKL57",
    "X_QZL21F", "X_QZLN56", "X_QZP026", "X_QZS215", "X_QZT8ZY", "X_QZTHFH", "X_RZ5YV3",
    "X_RZ85SF", "X_RZ94HV", "X_RZ9FHY", "X_RZ9GLM", "X_RZC84R", "X_RZQQC9", "X_RZS4LR",
    "X_RZWHM5", "X_RZXPB8", "X_SZ07Z9", "X_SZ0RMD", "X_SZB8KS", "X_SZCSRK", "X_SZCXZQ",
    "X_SZD9C5", "X_SZDQ69", "X_SZFBST", "X_SZFC5H", "X_SZJ2KG", "X_SZJ30M", "X_SZJXHK",
    "X_SZPWM9", "X_TZ00ND", "X_TZ57QZ", "X_TZ6244", "X_TZ9R72", "X_TZBTNC", "X_TZDGYJ",
    "X_TZKFSX", "X_TZQ3DT", "X_TZSYZD", "X_TZVTDC", "X_VZ0KRK", "X_VZ6G8X", "X_VZ88FZ",
    "X_VZ9R4W", "X_VZBJ66", "X_VZDSPX", "X_VZHVYQ", "X_VZM6JN", "X_VZWFFN", "X_VZYWF2",
    "X_WZ0203", "X_WZKD2W", "X_WZKV49", "X_WZS4K1", "X_WZV72Q", "X_XZ0XH2", "X_XZ3MFB",
    "X_XZ51PJ", "X_XZ9XRW", "X_XZJ5BC", "X_XZTKK0", "X_XZTX1V", "X_ZZ2QL6", "X_ZZ6CXJ",
    "X_ZZ6L7C", "X_ZZB08G", "X_ZZQ6K0", "X_ZZVMWQ"
  ]
}

team_workspaces = {
  ws_type     = "t"
  root_prefix = "team"
  team_configs = {
    TECH-PLTFRM-AWS = {
      name        = "TECH-PLTFRM-AWS"
      env_scope   = ["innov", "dev", "prod"]
      s3_prefixes = ["TECH_PLATFORM_AWS"]
    }
    AUTO-BUS-OPTI = {
      name        = "AUTO-BUS-OPTI"
      env_scope   = ["prod"]
      s3_prefixes = ["AUTO_BUSINESS_OPTIMIZATION"]
    }
    AUTO-DSS = {
      name        = "AUTO-DSS"
      env_scope   = ["dev", "prod"]
      s3_prefixes = ["AUTO_DSS"]
    }
    AUTO-DSS-CS-LP = {
      name        = "AUTO-DSS-CS-LP"
      env_scope   = ["prod"]
      s3_prefixes = ["AUTO_DSS_CAM_CS_LP"]
    }
    AUTO-DSS-DIG = {
      name        = "AUTO-DSS-DIG"
      env_scope   = ["dev", "sit", "model", "cap", "prod"]
      s3_prefixes = ["AUTO_DSS_DIGITAL"]
    }
    AUTO-INSU-SPA = {
      name        = "AUTO-INSU-SPA"
      env_scope   = ["prod"]
      s3_prefixes = ["AUTO_INSURANCE_SPA"]
    }
    TECH-CIH = {
      name        = "TECH-CIH"
      env_scope   = ["dev", "sit", "prod"]
      s3_prefixes = ["TECH_CIH"]
    }
    TECH-QMA = {
      name        = "TECH-QMA"
      env_scope   = ["prod"]
      s3_prefixes = ["TECH_QMA"]
    }
    TECH-MEAD = {
      name        = "TECH-MEAD"
      env_scope   = ["dev", "prod"]
      s3_prefixes = ["TECH_MEAD"]
    }
    TECH-PLTFRM-NUC = {
      name        = "TECH-PLTFRM-NUC"
      env_scope   = ["dev", "prod"]
      s3_prefixes = ["TECH_PLATFORM_NUCLEUS"]
    }
    # Next team's config should be placed here
  }
}

s3_sub_prefixes = ["WORK", "TEST", "CORE", "STATIC"]
