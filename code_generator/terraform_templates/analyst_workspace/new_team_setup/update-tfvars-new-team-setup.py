#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse, sys, csv
import hcl

# Function which initializes the variables needed for execution
def initialize_variables():
  global program_params, out_data, args

  parser = argparse.ArgumentParser()
  parser.add_argument('-t', '--type', choices=['team_s3_prefix', 'user_s3_prefix', 'sfc_iam_role'],
                      help = 'Provide the type of AWS resource for which .tfvars need to be updated')
  args = parser.parse_args()

  program_params = {
    'params_file_name': 'analyst-new-team-setup.param',
    'tf_file_name': 'terraform.tfvars',
    'out_file_name': 'output.tfvars',
    'env_map': {
      'I': {'env_name': 'dev', 'sdlc_name': 'innov'},
      'D': {'env_name': 'qa', 'sdlc_name': 'dev'},
      'S': {'env_name': 'qa', 'sdlc_name': 'sit'},
      'M': {'env_name': 'qa', 'sdlc_name': 'model'},
      'C': {'env_name': 'qa', 'sdlc_name': 'cap'},
      'P': {'env_name': 'prod', 'sdlc_name': 'prod'}
    }
  }
  out_data = ''


# Function to update Terraform vars dictionary with the input new teams or users - For S3 prefixes
def update_tf_vars_s3_prefix(tf_data, params_list, prefix_type):
  global program_params

  if prefix_type == 'teams':
    for params in params_list:
      team_dict = {}
      team_dict['name'] = params['team_short_name']
      team_dict['env_scope'] = []

      env_list = params['env_names'].split(',')
      for index, env in enumerate(env_list, start=1):
        sdlc_name = program_params['env_map'][env]['sdlc_name']
        team_dict['env_scope'].append(sdlc_name)

      team_dict['s3_prefixes'] = params['team_name'].split(',')
      tf_data['team_workspaces']['team_configs'][params['team_short_name']] = team_dict

    return tf_data
  elif prefix_type == 'users':
    user_s3_prefixes = tf_data['user_workspaces']['s3_prefixes']
    for params in params_list:
      user_s3_prefixes.append('X_' + params['user_zid'].upper())

    user_s3_prefixes.sort()
    user_s3_prefixes = list(dict.fromkeys(user_s3_prefixes))
    tf_data['user_workspaces']['s3_prefixes'] = user_s3_prefixes

    return tf_data


# Function to update Terraform vars dictionary with the input new teams - For Snowflake IAM Roles
def update_tf_vars_sfc_iam_role(tf_data, params_list):
  global program_params

  for params in params_list:
    team_dict = {}
    team_dict['name'] = params['team_short_name']

    team_dict['env_scope'] = {}
    team_dict['env_scope']['dev'] = []
    team_dict['env_scope']['qa'] = []
    team_dict['env_scope']['prod'] = []

    env_list = params['env_names'].split(',')
    for index, env in enumerate(env_list, start=1):
      env_name = program_params['env_map'][env]['env_name']
      sdlc_name = program_params['env_map'][env]['sdlc_name']
      team_dict['env_scope'][env_name].append(sdlc_name)

    team_dict['s3_prefixes'] = params['team_name'].split(',')
    team_dict['sf_ro_external_id'] = {'dev': [], 'qa': [], 'prod': []}
    team_dict['sf_rw_external_id'] = {'dev': [], 'qa': [], 'prod': []}
    tf_data['team_workspaces']['team_configs'][params['team_short_name']] = team_dict

  return tf_data


# Function to convert Terraform vars dictionary to .tfvars file
def convert_dict2hcl(tf_data, indent):
  global program_params, out_data

  spaces = indent * ' '
  for tf_var_name, tf_var_value in tf_data.items():
    if isinstance(tf_var_value, str):
      out_data = out_data + '\n' + spaces + tf_var_name + ' = "' + tf_var_value + '"'
    elif isinstance(tf_var_value, list):
      list_size = len(tf_var_value)
      if list_size > 2:
        out_data = out_data + '\n' + spaces + tf_var_name + ' = ['
        spaces = (indent + 2) * ' '
        for list_index, list_item in enumerate(tf_var_value, start=1):
          out_data = out_data + '\n' + spaces + list_item.join(['"', '"'])
          if list_index < list_size:
            out_data = out_data + ','
        spaces = indent * ' '
        out_data = out_data + '\n' + spaces + ']'
      elif list_size == 0:
        out_data = out_data + '\n' + spaces + tf_var_name + ' = []'
      else:
        out_data = out_data + '\n' + spaces + tf_var_name + ' = ["' + '", "'.join(tf_var_value) + '"]'
    elif isinstance(tf_var_value, dict):
      out_data = out_data + '\n' + spaces + tf_var_name + ' = {'
      indent = indent + 2
      convert_dict2hcl(tf_var_value, indent)
      out_data = out_data + '\n' + spaces + '}'
      indent = indent - 2

    if indent == 0:
      out_data = out_data + '\n'

  with open(program_params['out_file_name'], 'w') as outfile:
    outfile.write(out_data)


# Main function that is invoked first
def main():
  global program_params, args

  initialize_variables()

  tf_file_name = program_params['tf_file_name']

  # Convert TFVARS to DICT
  tf_data = {}
  with open(tf_file_name, 'r') as tf_file:
    tf_data = hcl.load(tf_file)

  # Read the input paramters in a list of dictionaries
  params_list = []
  with open(program_params['params_file_name'], 'r') as param_file:
    for params in csv.DictReader(param_file):
      params_list.append(params)

  # Update the DICT based on the input parameters
  if args.type == 'team_s3_prefix':
    tf_data = update_tf_vars_s3_prefix(tf_data, params_list, 'teams')
  elif args.type == 'user_s3_prefix':
    tf_data = update_tf_vars_s3_prefix(tf_data, params_list, 'users')
  elif args.type == 'sfc_iam_role':
    tf_data = update_tf_vars_sfc_iam_role(tf_data, params_list)
  else:
    print('Insufficient arguments provided ! So exiting the program without action...')
    sys.exit()

  # Save the output in .tfvars format
  convert_dict2hcl(tf_data, indent=0)


# Python program execution begins here
if __name__ == "__main__":
  main()
