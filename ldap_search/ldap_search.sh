#!/bin/sh

# This script reads the list of users from a parameter file,
# searches the Active Directory for the user's presence,
# saves the userID in the output file if a match is found.

input_file_name="input.params"
output_file_name="ldap_output.txt"
ldap_server="sat1mspdc002.nao.global.gmacfs.com"
ldap_port="389"
base_search="DC=NAO,DC=global,DC=gmacfs,DC=com"
your_z_id=$1

while read xid
do
  ldapsearch -LLL -x -b ${base_search} -H ldap://${ldap_server}:${ldap_port} -D "CN=${your_z_id},OU=People,OU=EUC,${base_search}" -w `cat .random` "sAMAccountName=${xid}" sAMAccountName | grep sAMAccountName | cut -d" " -f2 >> ${output_file_name}
done < ${input_file_name}
